/**
 * @file leds_class_ry.h
 *  
 * @brief 
 * 
 *        
 *
 * @copyright This file create by rensuiyi ,all right reserve!
 * 
 * @author rensuiyi
 * 
 * @date 2/20/20 3:42 PM
 */
#ifndef __LEDS_CLASS_RY_H__
#define __LEDS_CLASS_RY_H__

#ifdef __cplusplus
extern "c"
{
#endif /* __cplusplus */
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/leds.h>

extern int leds_class_ry_create(void);
extern int leds_class_ry_register(struct device* pparent, struct led_classdev* pclassdev);
extern void leds_class_ry_unregister(struct led_classdev* pclassdev);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __LEDS_CLASS_RY_H__ */


