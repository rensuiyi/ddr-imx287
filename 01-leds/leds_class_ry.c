/**
 * @file leds_class_ry.c
 *
 * @copyright This file create by rensuiyi ,all right   reserve!
 * 
 * @author rensuiyi
 * 
 * @date 2/20/20 3:43 PM
 */
#include <linux/err.h>

#include "leds_class_ry.h"

static struct class* m_pleds_class = NULL;

static ssize_t led_hello_show(struct device* dev, struct device_attribute* attr, char* buf) {
  return sprintf(buf, "hello:%s\n", "me");
}

static struct device_attribute led_dev_attrs[] = {
  __ATTR(hello, 0444, led_hello_show, NULL),
  __ATTR_NULL,
};


static ssize_t led_cls_hello_show(struct class* class, struct class_attribute* attr,
                                  char* buf) {
  return sprintf(buf, "cls:%s\n", "ok");
}

static struct class_attribute led_class_attrs[] = {
  __ATTR(ryhel, 0444, led_cls_hello_show, NULL),
  __ATTR_NULL,
};

CLASS_ATTR(ryhel, 0444, led_cls_hello_show, NULL);

static struct class ry_led_class = {
  .name =		"ryleds",
  .owner =	THIS_MODULE,
  .class_attrs =	led_class_attrs,
};

static int leds_class_create(void) {
  struct class* pclass = NULL;
  if (m_pleds_class!=NULL) {
    return 0;
  }
  //create the class in /sys/class/ryleds
  //pclass = class_create(THIS_MODULE, "ryleds");
  //if (IS_ERR(pclass)) {
  //  return PTR_ERR(pclass);
  //}
  class_register(&ry_led_class);
  pclass = &ry_led_class;
  printk(KERN_DEBUG "create the top class ryleds");
  // fill the class
  // pclass->dev_attrs = led_dev_attrs;
  pclass->class_attrs = led_class_attrs;
  
  m_pleds_class = pclass;
  return 0;
}



int leds_class_ry_register(struct device* pparent, struct led_classdev* pclassdev) {

  // create the top class
  if (0 != leds_class_create() ) {
    return -ENODEV;
  }

  return 0;
  // create the device and and the device to the class
  pclassdev->dev = device_create(m_pleds_class, pparent, 0, pclassdev, "%s", pclassdev->name);

  if (IS_ERR(pclassdev->dev)) {
    printk(KERN_DEBUG "create devcie failed\n");
    return PTR_ERR(pclassdev->dev);
  }


  printk(KERN_DEBUG "Resgistered led device:%s\n", pclassdev->name);

  return 0;
}

void leds_class_ry_unregister(struct led_classdev* pclassdev) {
  if (m_pleds_class!=NULL ) {
    printk(KERN_DEBUG "destory top class");
    class_destroy(m_pleds_class);
    m_pleds_class = NULL;
  }
}
